FROM ubuntu:latest
RUN apt-get -y update && apt-get -y upgrade
RUN apt-get -y install openjdk-8-jdk wget
RUN java -version
RUN mkdir /usr/local/tomcat
RUN wget https://downloads.apache.org/tomcat/tomcat-8/v8.5.53/bin/apache-tomcat-8.5.53.tar.gz -O /tmp/tomcat.tar.gz
RUN cd /tmp && tar xvfz tomcat.tar.gz
RUN cp -Rv /tmp/apache-tomcat-8.5.53/* /usr/local/tomcat/
RUN ls /tmp
RUN ls /var/lib/docker-engine
RUN ls /usr/local/
COPY ./target/gameoflife.war /usr/local/tomcat/webapps
RUN ls /usr/local/tomcat/bin
EXPOSE 8080
CMD /usr/local/tomcat/bin/catalina.sh run
